Simple petit compteur permettant de compter les jours qui nous séparent d'un évènement marquant.

**Démo**

Utilisé actuellement pour honorer la présence de notre ministre de l'intérieur https://jcfrog.com/darmaday

**Techno**

featuring html, bootstrap, javascript, jQuery